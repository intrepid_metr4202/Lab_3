function returnArray = imFindDominos(turntabling, base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,TORQUE_EN_BYTE, MOVING_BYTE)

warning('off', 'all');

returnArray = [];

clear cam;
ref_image = imread('sergiy23.png');
cam = webcam(2);
cam.Resolution = '1920x1080';
cam.Brightness = 100;
pause(2);
ref_image = snapshot(cam);
tic;
oldRef = ref_image;
% fiducial mask
mask = poly2mask([1300 1750 1750 1300 1300], [450 450 1080 1080 450], 1080, 1920);
maskedRef = uint8(mask);
maskedRGB = uint8(zeros(size(maskedRef)));
maskedRGB(:,:,1) = ref_image(:,:,1).*maskedRef;
maskedRGB(:,:,2) = ref_image(:,:,2).*maskedRef;
maskedRGB(:,:,3) = ref_image(:,:,3).*maskedRef;
fiducial_mask = maskedRGB;
% boneyard
if turntabling == 2
    mask = poly2mask([100 950 950 100 100], [450 450 1080 1080 450], 1080, 1920);
    maskedRef = uint8(mask);
    maskedRGB = uint8(zeros(size(maskedRef)));
    maskedRGB(:,:,1) = ref_image(:,:,1).*maskedRef;
    maskedRGB(:,:,2) = ref_image(:,:,2).*maskedRef;
    maskedRGB(:,:,3) = ref_image(:,:,3).*maskedRef;
    ref_image = maskedRGB;
    figure;imshow(ref_image);
else
% turntable
    mask = poly2mask([850 1350 1350 850 850], [550 550 1080 1080 550], 1080, 1920);
    maskedRef = uint8(mask);
    maskedRGB = uint8(zeros(size(maskedRef)));
    maskedRGB(:,:,1) = oldRef(:,:,1).*maskedRef;
    maskedRGB(:,:,2) = oldRef(:,:,2).*maskedRef;
    maskedRGB(:,:,3) = oldRef(:,:,3).*maskedRef;
    ref_image = maskedRGB;
    figure;imshow(ref_image);
end
% HSV image
A = rgb2hsv(ref_image);
greenHSV = A(:,:,2);
greenHSV = imgaussfilt(greenHSV, 0.55);

grayIm = rgb2gray(ref_image);

% use checkerboard as a fiducal for world coordinates
centers = detectCheckerboardPoints(fiducial_mask);
radii = ones(size(centers, 1),1);
jessesCenters = sortrows(centers, [2 1]);
% the corner of the checkerboard fiducial
jessesPoint = jessesCenters(1,:);

R= ref_image(:,:,1);
G= ref_image(:,:,2);
BlueChannel= ref_image(:,:,3); 
imshow(BlueChannel);
% GAUSSIAN BLUR
B = imgaussfilt(BlueChannel);
[regions,mserCC] = detectMSERFeatures(B);

figure; imshow(B);
hold on; viscircles(centers, radii,'EdgeColor','g');
stats = regionprops('table',mserCC,'MajorAxisLength', 'MinorAxisLength', 'Eccentricity');
% pick out black bar inside dominos - length/width from 8 to 13
minorandmajorindx = stats.MajorAxisLength./stats.MinorAxisLength > 8 & stats.MajorAxisLength./stats.MinorAxisLength < 13 & stats.Eccentricity > 0.95 & stats.Eccentricity < 0.9999; 
dominoeRegions = regions(minorandmajorindx);

% detect pips
[regions,mserCC] = detectMSERFeatures(B);
stats = regionprops('table',mserCC,'MajorAxisLength', 'MinorAxisLength', 'Eccentricity');
% pick out roughly circular pips with ellipse eccentricity less than 9 and length/width between 0.3 and 1.9
dominoPips = stats.Eccentricity < 0.9 & stats.MajorAxisLength./stats.MinorAxisLength > 0.3 & stats.MajorAxisLength./stats.MinorAxisLength < 1.9;
pipRegions = regions(dominoPips);

passedDominoIdx = [];
xCentroidDistThres = 20;
yCentroidDistThres = 10;

checkedDominoIdx = [];
% make sure we don't get crowded ellipses more than once
for i=1:size(dominoeRegions,1)
    location = dominoeRegions(i).Location;
    xLoc = location(1);
    yLoc = location(2);
    validDomino = 1;
    tempIdx = [i];
    tempLongAxes = [dominoeRegions.Axes(i,1)];
    % any ellipses whose centroid is within 35% of the length of the current potential black bar is considered a duplicate
    centroidDistThres = 0.35*tempLongAxes;
    for j=1:size(dominoeRegions, 1)
        if j~=i
            location2 = dominoeRegions(j).Location;
            distanceCentroidsVec = location2 - location;
            distanceCentroids = sqrt(distanceCentroidsVec*distanceCentroidsVec');
            % orientation is probably unneeded, given that
            % centroidDistThres = 0.5*tempLongAxes is basically guaranteed
            % to not accidentally filter out a domino because of the
            % geometry of the domino and its black bar. In fact could
            % probably up it to 0.9
%             orientation2 = dominoeRegions(j).Orientation;
%             if orientation2>orientation1
%                 orientationRatio = orientation2/orientation1;
%             else
%                 orientationRatio = orientation1/orientation2;
%             end
            %if (abs(xLoc-location2(1))<xCentroidDistThres && abs(yLoc-location2(2))<yCentroidDistThres)
            if (distanceCentroids < centroidDistThres)% && orientationRatio < 0.99 && orientationRatio > 1.01
                tempIdx = [tempIdx; j];
                tempLongAxes = [tempLongAxes; dominoeRegions.Axes(j,1)];
            end
        end
    end
    [B,newIdx] = sort(tempLongAxes, 'ascend');
    % choose the median size ellipse out of the crowded duplicates
%     if mod(length(tempIdx),2)==0 % even
%         nth = length(newIdx)/2+1;
%     else
%         nth = (length(newIdx)-1)/2+1;
%     end
    % just go for largest
    nth = length(newIdx);
    dominoIdx = tempIdx(nth);
    if ~(any(dominoIdx==checkedDominoIdx))
        passedDominoIdx = [passedDominoIdx; dominoIdx];
    end
    checkedDominoIdx = [checkedDominoIdx; tempIdx];
end
dominoeRegions = dominoeRegions(passedDominoIdx);
passedDominoIdx = [];
% find pips
listOfDominos = [];
for i=1:size(dominoeRegions,1)
    tempDomino = detectPips(dominoeRegions, i, pipRegions, BlueChannel, greenHSV, turntabling);
    if ~isempty(tempDomino)
        listOfDominos = [listOfDominos; tempDomino];
        passedDominoIdx = [passedDominoIdx; i];
    end
end
dominoeRegions = dominoeRegions(passedDominoIdx);
if ~isempty(listOfDominos)
    currentDominoLocations = listOfDominos(:,[1:2]);
    [currentDominoLocations, sortedIdx] = sortrows(currentDominoLocations,[2 1]);
    % negative angle means turn end effector clockwise
    dominoAngles = listOfDominos(:,3);
    dominoAngles = dominoAngles(sortedIdx);
    dominoAnglesTarget = listOfDominos(:,6);
    dominoAnglesTarget = dominoAnglesTarget(sortedIdx);
    dominoValues = listOfDominos(:,[4:5]);
    dominoValues = dominoValues(sortedIdx,:);
else
    currentDominoLocations = [];
    dominoAngles = [];
    dominoValues = [];
    return
end

Ends = [];
for i=1:size(currentDominoLocations,1)
    endXY = getSortedDominoLocation(dominoValues(i,:));
    endAngle = dominoAnglesTarget(i);
    Ends = [Ends; endXY -15 endAngle];
end

% World Coordinates
imageSize = size(ref_image);
xWorldLimits = [0 480*16/9];
yWorldLimits = [0 480];
RA = imref2d(imageSize, xWorldLimits, yWorldLimits);

disttoFidEndX = -285;
disttoFidEndY = -206;

fidX =  jessesPoint(1);
fidY =  jessesPoint(2);


[fidWorldX, fidWorldY] = intrinsicToWorld(RA,fidX,fidY);
locStr2 = sprintf('xI: %1.f yI: %1.f\n xW: %1.f yW: %1.f', fidX, fidY, fidWorldX, fidWorldY);
text(fidX, fidY, locStr2,'FontSize', 10, 'Color', 'green', 'HorizontalAlignment', 'center');

worldCoords = [];
for i=1:size(currentDominoLocations, 1)
    xCoords = currentDominoLocations(i,1);   
    yCoords = currentDominoLocations(i,2);    
    [xWorld,yWorld] = intrinsicToWorld(RA,xCoords,yCoords);
    
    xWorld = xWorld - fidWorldX;
    yWorld = yWorld - fidWorldY;

    xWorld = disttoFidEndX - xWorld;
    yWorld = disttoFidEndY - yWorld;
    
    % switch the x and y because workspace changed late in project
    yWorld = -yWorld;
    temp = xWorld;
    xWorld = yWorld;
    yWorld = temp;
    
    worldCoords = [worldCoords; xWorld yWorld];
    
    locStr = sprintf('xI: %1.f yI: %1.f\n xW: %1.f yW: %1.f', xCoords, yCoords, xWorld, yWorld);
    text(double(currentDominoLocations(i,1)), double(currentDominoLocations(i,2)), locStr,'FontSize', 10, 'Color', 'blue', 'HorizontalAlignment', 'center');
end

Dominoes = [];
for i=1:size(worldCoords,1)
    worldXY = worldCoords(i,:);
    worldAngle = dominoAngles(i);
    Dominoes = [Dominoes; worldXY -15 worldAngle];
end

returnArray = [Dominoes(1,:) Ends(1,:)];

% turntable
if turntabling == 1
    domPosX = double(worldCoords(:,1));
    domPosY = double(worldCoords(:,2));
    
    armPosX = 205;
    armPosY = -80;
    armPosZ = 125;
    
    % move the motor close to turntable
    GoalA = moveto([armPosX,70, armPosZ, 0], base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,TORQUE_EN_BYTE);
    wait(GoalA, base_ID,fst_ID, snd_ID, end_ID, MOVING_BYTE);
    linear_move([armPosX,70, armPosZ, 0], [armPosX,armPosY, armPosZ, 0],base_ID,fst_ID,snd_ID,end_ID,POS_BYTE, MOVING_BYTE,TORQUE_EN_BYTE, 0);
    
    pause(2);
    
    tableAngVel = (1/7.5)*2*pi;

    while 1
        timeElapsed = toc;
        angleTraveled = tableAngVel * timeElapsed;
        for i=1:length(domPosX)
            tempX = domPosX(i);
            tempY = domPosY(i);
            domPosX(i) = tempX * cos(angleTraveled) - tempY * sin(angleTraveled);
            domPosY(i) = tempY * cos(angleTraveled) + tempX * sin(angleTraveled);
            points = [domPosX(i) domPosY(i); armPosX armPosY];
            distance = pdist(points, 'Euclidean');
            
            if distance < 110
				% take domino off turntable
                linear_move([armPosX,armPosY, armPosZ, 0], [armPosX,armPosY, armPosZ-0, 0],base_ID,fst_ID,snd_ID,end_ID,POS_BYTE, MOVING_BYTE,TORQUE_EN_BYTE, 0);
                pause(1);
                linear_move([armPosX,armPosY, armPosZ-0, 0],[-200,200,75,0],base_ID,fst_ID,snd_ID,end_ID,POS_BYTE, MOVING_BYTE,TORQUE_EN_BYTE, 0);
                pause(1);
                return
            end
        end
    end    
end

% old blob detection stuff, didn't have enough time to implement obstacle avoidance

% % % % mask = poly2mask([975 1800 1800 975 975], [0 0 375 375 0], 1080, 1920);
% % % % maskedRef = uint8(mask);
% % % % maskedRGB = uint8(zeros(size(maskedRef)));
% % % % maskedRGB(:,:,1) = oldRef(:,:,1).*maskedRef;
% % % % maskedRGB(:,:,2) = oldRef(:,:,2).*maskedRef;
% % % % maskedRGB(:,:,3) = oldRef(:,:,3).*maskedRef;
% % % % ref_image = maskedRGB;
% % % % figure;imshow(ref_image);
% % % % 
% % % % 
% % % % %BW_blacked_out = cat(3, red, green, blue);
% % % % %figure; imshow(BW_blacked_out);
% % % % % ----- jesse blacked out dominos
% % % % %'x' is number of pixels to remove
% % % %     x = 1000 ;
% % % %     %'t' is sensitivity
% % % %     t = 0.25; 
% % % %     %blob_Im is filtering the ref_image differently for blob analysis
% % % %     blob_im = imgaussfilt(ref_image,1);
% % % %     %Gets the blue grayscale of blob_im
% % % %     blue_blob_im = blob_im(:,:,3);
% % % %     
% % % %     %figure,
% % % %     %imshow(y);
% % % %     %y = imgaussfilt(y,2);
% % % %     %BinaryDom1= y(:,:,3);
% % % %     %BinaryDom1 = histeq(rgb2gray(y));
% % % %     %imshow(I)
% % % %     
% % % %     BinaryDom =imbinarize(blue_blob_im,'adaptive','sensitivity', t);    
% % % %     BinaryDom = bwareaopen(BinaryDom, x);
% % % %     BinaryDom = bwmorph(BinaryDom, 'clean');
% % % %        
% % % % 	    
% % % %     
% % % %     %Objectidx=find((20000< [stats.Area]) & ([stats.Area] < 30000));
% % % %     %Run this through a for loop that uses the x,y of four corners of each
% % % %     %rectangle
% % % %     x = [];
% % % %     y = [];
% % % %     %Below blackens the area selected by the four corners on the binary
% % % %     %image
% % % %     BW_objects=BinaryDom;
% % % % %     for i = 1:length(centroidLocationsDominos) % num of dominos
% % % % %         x = [1, 500, 1000, 500, 1]; %Use indexing to get these coords
% % % % %         y = [600, 600, 500, 400, 600];
% % % % %         BW_objects = imcomplement(poly2mask(x,y,1080,1920)).*BinaryDom; 
% % % % %     end
% % % % %     for i = 1:size(xPlot1Array, 1)
% % % % %         roi_domino1 = roipoly(BW_objects, xPlot1Array(i,:), yPlot1Array(i,:));
% % % % %         BW_objects(roi_domino1) = 0;
% % % % %         roi_domino2 = roipoly(BW_objects, xPlot2Array(i,:), yPlot2Array(i,:));
% % % % %         BW_objects(roi_domino2) = 0;
% % % % %     end
% % % %     figure,
% % % %     imshow(BW_objects);
% % % %     
% % % %     
% % % %     % run blob detection
% % % %     CC = bwconncomp(BW_objects); 
% % % %     no_obj = CC.NumObjects; 
% % % %     stats = regionprops(CC, 'Centroid', 'Area', 'BoundingBox');
% % % %         
% % % %     %Draw boxes around every area of interest
% % % %     for i=1:length(stats) 
% % % %     interestArea = stats(i).BoundingBox;
% % % %         rectangle('Position',interestArea,'EdgeColor','blue');    
% % % %     end
% % % % % ----- end jesse


%end
