Although a quick heatmap of the commits will show a concentration for only two team members, this does not reflect the fact that the other three team members helped with mechanical design and build and other general problem solving. 



A video demonstration and in-depth exploration of the inner workings of this project is available at: https://www.youtube.com/watch?v=pQVrA_j-i8A



This is our team's git repository for Lab 3 of METR4202 2016 at The University of Queensland - the university for the complex world. We chose to sort dominos. 



The computer vision works by detecting maximally extermal stable regions (MSER). The MSER features are then used to detect dominos accurately with no false positives. The middle black bar of the domino is always enclosed in the white surface area of the domino which meant that the black bar was detected as an MSER under nearly every lighting condition. By filtering out ellipses that don't satisfy the length vs width ratio of the black bar (approx 8 to 13), some false positives and also some duplicates are still left. Multiple ellipses very close together were filtered out by using the knowledge that the because the dominos are not stacked, the minimum distance between the centroids of valid dominos is at least 1X the length of the black bar, which equals the major axis length of the detected ellipses.

From then on, to further refine the detection, the known and fixed geometries of each domino was exploited - ratio of black bar’s dimensions, ratios of domino dimensions relative to black bar, ratio of pip area to black bar’s area and ratio of pip x-y locations to black bar’s dimensions and location. Particularly, the geometric shape/pattern of the detected pips inside each half of each domino was correlated to the known geometry for its detected value, where invalid geometries for the detected number of pips indicated a false domino detection. This helped filter out any false positives that had passed the previous tests. All of this detection is enabled by exploiting information that is known about the desired objects to be detected and is information that is highly unlikely to change under different lighting and scale conditions. 

The main CV code file is imFindDominos.m. 


The robot arm consists of 3 revolute joints for movement with a fourth on the bottom as an end effector to match the pose of the detected dominos. A four-bar linkage mechanism mechanism was used keep the end effector orthogonal to the workspace at all times. The motors were controlled using the official Dynamixel MATLAB SDK. 

Inverse kinematics was used to calculate the angles required for the 3 joints in order to move to the desired location of a detected domino. These angles were then sent to the Dynamixel motors. A sorted domino pattern was chosen such that there are lanes so that dominos do not have to be sorted sequentially. Straight line motion was approximated by breaking up curves into series of smaller curves in motorMovement/discretising.m. 

The main motor code file is motorMovement/Funa.m. It runs imFindDominos.m and proceeds to move the arm to move dominos from the boneyard/turntable to the sorting area. 
