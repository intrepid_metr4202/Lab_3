
function outArray = getSortedDominoLocation(dominoValue)
    startingDominoX = 180; 
    dominoWidth = 25;
    dominoSpacingEachSideWidth = 15;
    numFirstRow = 10;
    numPerRow = 9;
    dominoLength = 51;
    dominoSpacingEachSideLength = 67.5;
    xSortedCoords = [];
    ySortedCoords = [];
    xRow = [];
    startingDominoY = 350;
    yTwo = 220;
    yThree = 90;
    ySortedCoords = [startingDominoY yTwo yThree];
    for i=numFirstRow:-1:1
        xCurrentCoord = startingDominoX - (i-1)*dominoWidth - (i-1)*dominoSpacingEachSideWidth;
        xRow = [xRow xCurrentCoord];
    end
    xSortedCoords = [xSortedCoords; xRow];
    xRow = [0];
    for i=numPerRow:-1:1
        xCurrentCoord = startingDominoX - (i-1)*dominoWidth - (i-1)*dominoSpacingEachSideWidth;
        xRow = [xRow xCurrentCoord];
    end
    xSortedCoords = [xSortedCoords; xRow; xRow];
    dominoMin = dominoValue(1);
    dominoMax = dominoValue(2);
    previousMinDominos = 7 + (dominoMin - 1)*-1; % arithmetic progression a_n
    sumMinDominos = (7 + previousMinDominos)*dominoMin*0.5; % arithmetic progression S_n
    dominoOrder = sumMinDominos + dominoMax-dominoMin;
    if dominoOrder < 10
        dominoRow = 1;
        dominoColumn = dominoOrder;
    elseif dominoOrder >= 10 && dominoOrder < 19
        dominoRow = 2;
        dominoColumn = dominoOrder - 10;
    elseif dominoOrder >= 19 && dominoOrder <= 28
        dominoRow = 3;
        dominoColumn = dominoOrder - 19;
    end
    xDominoPosition = xSortedCoords(dominoRow, 10-dominoColumn);
    yDominoPosition = ySortedCoords(dominoRow);
    % switch the x and y because workspace changed late in project
    outArray = [-yDominoPosition -xDominoPosition];
end
