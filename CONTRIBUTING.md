Team Member Contributions: 



Initial Mechanical Design: Andrew, Meetkumar

Mechanical build and optimisation: Andrew

Actuation (robot control): Meetkumar

Feature detection: Jesse

Domino detection and locating: Ryan (with help from Sergiy)

Sorting algorithm: Jesse, Meetkumar, Andrew

Obstacle avoidance: Sergiy
