close all
clear all

loadlibrary('dynamixel', 'dynamixel.h');
libfunctions('dynamixel');

DEFAULT_PORTNUM = 3; %COM3
DEFAULT_BAUDNUM = 1; %1Mbps

dyn = calllib('dynamixel','dxl_initialize',DEFAULT_PORTNUM,DEFAULT_BAUDNUM);
if (dyn == 0)
    'Init failed'
    return;
end

base_ID = 3;
fst_ID = 4;
snd_ID = 42;
end_ID = 69;

calllib('dynamixel','dxl_write_word',base_ID,34,818);
calllib('dynamixel','dxl_write_word',fst_ID,34,818);
calllib('dynamixel','dxl_write_word',snd_ID,34,818);

    %%% This is motor control code simillar to wut.m using the Dynamixel native SDK.
    %%% Define constants 

    % Motor IDS

    % default speeds
    DEFAULT_SPEED_AX = uint16(5*1023/114); %Speed = 10RPM
    % DEFAULT_SPEED_MX = uint16(10*1023/117.07); %Speed = 10RPM
    DEFAULT_SPEED_MX = uint16(2*1023/117.07); 

    % CONTROL TABLE values needed
    SPEED_BYTE = 32; % byte to write desired speed to
    POS_BYTE = 30; % byte to write pos to
    MOVING_BYTE = 36; %46 
    TORQUE_EN_BYTE = 24;
    POS_NOW_BYTE = 36;
    CW_ANGLE_BYTE = 6;
    CCW_ANGLE_BYTE = 8;
    MT_OFFSET_BYTE = 20;

    %HOW to communicate with Dytnamixel
    

    %Default Position for robot


    %% Load API


    %% Init

    % connect with dynamixel


    % set speeds for motors

    calllib('dynamixel','dxl_write_word',base_ID,SPEED_BYTE,DEFAULT_SPEED_AX);
    calllib('dynamixel','dxl_write_word',fst_ID,SPEED_BYTE,DEFAULT_SPEED_AX);
    calllib('dynamixel','dxl_write_word',snd_ID,SPEED_BYTE,DEFAULT_SPEED_AX);
    calllib('dynamixel','dxl_write_word',end_ID,SPEED_BYTE,DEFAULT_SPEED_MX);
    
    
    calllib('dynamixel','dxl_write_word',end_ID,CW_ANGLE_BYTE,4095);
    calllib('dynamixel','dxl_write_word',end_ID,CCW_ANGLE_BYTE,4095);

while 1
	% try and find dominos on turntable
    returnArray = imFindDominos(1, base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,TORQUE_EN_BYTE, MOVING_BYTE);
    if isempty(returnArray)
		% no dominos on turntable, move on to boneyard
        returnArray = imFindDominos(2, base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,TORQUE_EN_BYTE, MOVING_BYTE);
        if isempty(returnArray)
			% no dominos left, exit script
            break
        end
    else
		% domino found on turntable, skip rest of loop because imFindDominos will handle it
        continue
    end
    Dominoes = returnArray(1:4);
    % final position in sorted area
    Ends = returnArray(5:8);


    %%% Move to default position
	% pick a default x,y,z pos and use inv kin to move to i
	% maybe x = -100, y = 0, z = 100. EE = 0deg   
    DefaultPos = [-200,200,75,0];

    Goal = moveto(DefaultPos, base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,TORQUE_EN_BYTE);
    wait(Goal, base_ID, fst_ID, snd_ID, end_ID, MOVING_BYTE);
    pause(1);

    i = 1;
    while (i<size(Dominoes,1)+1)
        Start = Dominoes(i,:);
        
        Startat =[Start(1),Start(2),30+arc_interp(Start),Start(4)];%+arc_interp(Start)
        
        End = Ends(i,:);
        whatever = 0;
        if End(4)==180 || End(4)==-180
            whatever = 1;
        end
        Endat = [End(1),End(2),30+arc_interp(Start),End(4)];%+arc_interp(End)
        linear_move(DefaultPos,[0,250,100,0],base_ID,fst_ID,snd_ID,end_ID,POS_BYTE, MOVING_BYTE,TORQUE_EN_BYTE, whatever);
        pause(2);
        
        Goala = moveto(Startat, base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,TORQUE_EN_BYTE);
        wait(Goala, base_ID, fst_ID, snd_ID, end_ID, MOVING_BYTE);
		pause(2);
        linear_move(Startat,Start,base_ID,fst_ID,snd_ID,end_ID,POS_BYTE, MOVING_BYTE,TORQUE_EN_BYTE, whatever);
        pause(2);

        PATH(Start, End,base_ID,fst_ID,snd_ID,end_ID,POS_BYTE, MOVING_BYTE,TORQUE_EN_BYTE, whatever);

        linear_move(End,Endat,base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,MOVING_BYTE,TORQUE_EN_BYTE, whatever);
        pause(2);
        i = i+1;
    end
end
%%% Exit
calllib('dynamixel','dxl_terminate');
unloadlibrary('dynamixel');
