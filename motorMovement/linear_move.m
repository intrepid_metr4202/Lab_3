function linear_move(Start, End,base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,MOVING_BYTE,TORQUE_EN_BYTE, whatever)
    seq = discretising(Start,End, whatever);
    for i = 1:size(seq,1)
        %pause(0.1);
        Pos = seq(i,:);
        Goal = moveto(Pos, base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,TORQUE_EN_BYTE);
        %if (i == length(seq))
        wait(Goal, base_ID, fst_ID, snd_ID, end_ID, MOVING_BYTE);
        %end
    end
end 