function simult(base_ID,fst_ID, snd_ID, end_ID,POS_BYTE,TORQUE_EN_BYTE, ToSend)
    calllib('dynamixel','dxl_write_word',base_ID,POS_BYTE,ToSend(1));
    calllib('dynamixel','dxl_write_word',snd_ID,POS_BYTE,ToSend(3));
    calllib('dynamixel','dxl_write_word',fst_ID,POS_BYTE,ToSend(2));
    calllib('dynamixel','dxl_write_word',end_ID,POS_BYTE,ToSend(4));
end
