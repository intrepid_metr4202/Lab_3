function [ToSend2] = moveto(DefaultPos, base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,TORQUE_EN_BYTE)
    DefaultAngles = [invkine(DefaultPos(1:3))];
    EE = (DefaultAngles(1))+80 - DefaultPos(4)-25;
    ToSend = uint16(DefaultAngles*1023/300);
    ToSend2 = [ToSend, uint16(EE*2048/180)];
    simult(base_ID,fst_ID, snd_ID, end_ID,POS_BYTE,TORQUE_EN_BYTE,ToSend2);
end 