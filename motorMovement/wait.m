function wait(Goal, base_ID,fst_ID, snd_ID, end_ID, MOVING_BYTE)  
    GoalA = Goal(1);
    GoalB = Goal(2);
    GoalC = Goal(3);
    GoalD = Goal(4);
    
    runningDiffB = 0;
    runningDiffDiff = 0;
    runningTotal = 0;
    
   
    DiffA = abs(int32(GoalA) -  int32(calllib('dynamixel','dxl_read_word',base_ID,MOVING_BYTE)));
    DiffB = abs(int32(GoalB) -  int32(calllib('dynamixel','dxl_read_word',fst_ID,MOVING_BYTE)));
    DiffC = abs(int32(GoalC) -  int32(calllib('dynamixel','dxl_read_word',snd_ID,MOVING_BYTE)));
    DiffD = abs(int32(GoalD) -  int32(calllib('dynamixel','dxl_read_word',end_ID,MOVING_BYTE)));
    
    tic;
    while (1)
            DiffA = abs(int32(GoalA) -  int32(calllib('dynamixel','dxl_read_word',base_ID,MOVING_BYTE)));
            DiffB = abs(int32(GoalB) -  int32(calllib('dynamixel','dxl_read_word',fst_ID,MOVING_BYTE)));
            DiffC = abs(int32(GoalC) -  int32(calllib('dynamixel','dxl_read_word',snd_ID,MOVING_BYTE)));
            DiffD = abs(int32(GoalD) -  int32(calllib('dynamixel','dxl_read_word',end_ID,MOVING_BYTE)));
            
            if (DiffA < 15) && (DiffB < 15) && (DiffC < 15) && (DiffD < 110)
               break; 
            end
            if toc>3
                break;
            end
    end
end 
