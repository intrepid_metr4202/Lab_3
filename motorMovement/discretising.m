function [seq] = discretising(a,b, whatever)

% dl predef(tweak)
    dl = 5; %1cm for now

    x0 = a(1);
    y0 = a(2);
    z0 = a(3);
    th0 = a(4);

    x1 = b(1);
    y1 = b(2);
    z1 = b(3);
    th1 = b(4);

    x = x1-x0;
    y = y1-y0;
    z = z1-z0;
    th = th1-th0;

    l = sqrt(x^2 + y^2+ z^2);

    steps = floor(l/dl)+1;

    xcol = (linspace(x0,x1,steps)).';
    ycol = (linspace(y0,y1,steps)).';
    zcol = (linspace(z0,z1,steps)).';
    thcol = (linspace(th0,th1,steps)).';

    seq = [xcol,ycol,zcol,thcol];
    B = seq;
    B(1,:) = [];
    seq = B;    
end
