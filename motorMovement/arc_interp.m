function zoff = arc_interp(POS)
   x = POS(1);
   y = POS(2);
   z = POS(3);
   M = sqrt(x^2 +y^2);
   if (M > 160)
       zoff = M*(0.15);
   else 
       zoff = 0;
   end
   %zoff = M*(0.12);
end 