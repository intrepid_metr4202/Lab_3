function PATH(posInGrave, posInSort,base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,MOVING_BYTE,TORQUE_EN_BYTE, whatever)
    if (posInSort(1) <  -300)
        laneX = -287;
    else 
        laneX = -152;
    end
   
    intermediate = [-70,220,0,posInSort(4)];
    
    beforeLane = [laneX,220,0,posInSort(4)];
    
    laneIntermediate = [laneX,posInSort(2),0,posInSort(4)];
    
%     Goal = moveto(intermediate, base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,TORQUE_EN_BYTE);
    %if (i == length(seq))
%     wait(Goal, base_ID, fst_ID, snd_ID, end_ID, MOVING_BYTE);
    linear_move(posInGrave,intermediate,base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,MOVING_BYTE,TORQUE_EN_BYTE,whatever);
    pause(1);
    linear_move(intermediate,beforeLane,base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,MOVING_BYTE,TORQUE_EN_BYTE, whatever);
    pause(1);
    linear_move(beforeLane,laneIntermediate,base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,MOVING_BYTE,TORQUE_EN_BYTE, whatever);
    pause(1);
    linear_move(laneIntermediate,posInSort,base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,MOVING_BYTE,TORQUE_EN_BYTE, whatever);
    pause(1);
%     linear_move(posInSort,laneIntermediate,base_ID,fst_ID,snd_ID,end_ID,POS_BYTE,MOVING_BYTE,TORQUE_EN_BYTE, whatever);
%     pause(1);
end 