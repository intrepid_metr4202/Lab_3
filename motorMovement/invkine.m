function [angles] = invkine(pos)
% this fucntion calculates the angles for the robot connections based on 
% the true pos of the wrist.

%% definitions
% pos [x,y,z] = [0,0,0] is defined as the base of the robot directly under 
% the base rotation axis.
% direction x to the right of base
% direction y in forward direction
% direction z up

% angles [t0, t1, t2] = [0,0,0] is when robot is vertical and pointing to
% in its x direction.

% angles t0 is anticlockwise, t1 (test for yourself)

% constants:
% b = z offset from base to 2nd motor rotation
% c = radial offset from base rotation axis to 2nd motor rotation

b = 92-55-20;
c = 0;
l1 = 228; %228
l2 = 252;
%% kinematic equations

y = pos(1)-10 + 15;%-15
x = pos(2)+20;%+20
z = pos(3);

t0 = atan2(y,x); % base angle

d = sqrt(x^2 + y^2) - c;
z2 = z - b;

n = (d^2 + z2^2 - l1^2 - l2^2)/(2*l1*l2);
t2 = atan2(sqrt(1-n^2),n);
t1 = atan2(d,z2) - atan2(l2*sin(t2),l1+l2*cos(t2));

t0 = rad2deg(t0)+150+20; %-5.5
t1 = -rad2deg(t1)+150+5.5;
t2 = rad2deg(t2)-90+150;

if (t1>155)
    'ERROR: too far back'
    t1 = 155;
end

angles = [t0,t1,t2];
end











