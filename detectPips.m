function returnArray = detectPips(dominoeRegions, iter, pipRegions, B, imHSV, turntabling)
    % variables to return
    returnArray = [];
    % variables for calculations
    location = dominoeRegions(iter).Location;
    orientation = dominoeRegions(iter).Orientation;
    axes = dominoeRegions(iter).Axes;
    length = double(axes(1));
    width = double(axes(2));
    blackBarArea = length*width;
    pos1 = location + 0.575*[length*cos(-orientation) length*sin(-orientation)];
    pos2 = location - 0.575*[length*cos(-orientation) length*sin(-orientation)];
    angle = 90;
    Rot = [cosd(angle) -sind(angle); sind(angle) cosd(angle)];
    vector90 = (pos2 - pos1)*Rot;
    vector90Norm = vector90/norm(vector90);
    notDomino = 0;
    xPlotFirst = [];
    yPlotFirst = [];
    xPlotSecond = [];
    yPlotSecond = [];
    x_bar1 = 0;
    y_bar1 = 0;
    x_bar2 = 0;
    y_bar2 = 0;
    dominoValueFirst = 0;
    dominoValueSecond = 0;
    for i=1:2
        if i==1
            posNegMult = 1;
        else
            posNegMult = -1;
        end
        pos3 = pos2 + posNegMult*vector90Norm*1.175*length;
        pos4 = pos1 + posNegMult*vector90Norm*1.175*length;
        xPlot1 = [pos1(1) pos2(1) pos3(1) pos4(1) pos1(1)];
        yPlot1 = [pos1(2) pos2(2) pos3(2) pos4(2) pos1(2)];
        area = polyarea(xPlot1, yPlot1);
        if (area<3250 || area>4450) && turntabling==2
            return
        end
        if turntabling==1
            if (area<4300 || area>5100)
                return
            end
        end
        % find no. of pips in first half
        pipLocations = pipRegions.Location;
        pipLocations = pipLocations';
        xq1 = pipLocations(1,:);
        yq1 = pipLocations(2,:);
        in1 = inpolygon(xq1,yq1,xPlot1,yPlot1);
        inIndices1 = find(in1);
        currentPipLocations = [-99 -99];
        currentPipIdx = [];
        for j=1:size(inIndices1,2)
            idx = inIndices1(j);
            pipCurrentLocation = pipRegions(idx).Location;
            xLoc = pipCurrentLocation(1);
            yLoc = pipCurrentLocation(2);
            validPip = 1;
            axesPip = pipRegions(idx).Axes;
            lengthPip = axesPip(1);
            widthPip = axesPip(2);
            pipArea = lengthPip*widthPip;
            pipCentroidDistThres = 0.8*lengthPip;
            % I set the areaRatioThreshold looser because some pips were
            % detected with very small circles, and also because later
            % geometry checking should catch any false dominos; otherwise
            % could filter out duplicate pips by median size or largest
            % size etc
            % area of half domino = 25.5*25 = 637.5
            % area of pip square = 25
            % ratio = 25.5
            areaRatio = (polyarea(xPlot1, yPlot1))/pipArea;
            areaRatioThresholdLower = 10;
            areaRatioThresholdUpper = 40;
            for k=1:size(currentPipLocations, 1)
                if ((abs(xLoc-currentPipLocations(k,1))<pipCentroidDistThres && abs(yLoc-currentPipLocations(k,2))<pipCentroidDistThres) || (areaRatio<areaRatioThresholdLower || areaRatio>areaRatioThresholdUpper))
                    validPip = 0;
                    break;
                end
            end
            if validPip
                currentPipLocations = [currentPipLocations; xLoc yLoc];
                currentPipIdx = [currentPipIdx; idx];
            end
        end
        dominoValue1 = size(currentPipLocations,1) - 1; % there was -99, -99 at beginning
        if mod(dominoValue1,2) % 1, 3, 5
			% draw the polygon to test the center pip
            posTest1 = location + 0.2*[length*cos(-orientation) length*sin(-orientation)];
            posTest2 = location - 0.2*[length*cos(-orientation) length*sin(-orientation)];
            posTest2 = posTest2 + posNegMult*vector90Norm*0.35*length;
            posTest1 = posTest1 + posNegMult*vector90Norm*0.35*length;
            posTest4 = posTest1 + posNegMult*vector90Norm*0.425*length;
            posTest3 = posTest2 + posNegMult*vector90Norm*0.425*length;
            xPlotTest1 = [posTest1(1) posTest2(1) posTest3(1) posTest4(1) posTest1(1)];
            yPlotTest1 = [posTest1(2) posTest2(2) posTest3(2) posTest4(2) posTest1(2)];
            xqPip = (currentPipLocations(:,1))';
            yqPip = (currentPipLocations(:,2))';
            inPip = inpolygon(xqPip,yqPip,xPlotTest1,yPlotTest1);
            inIndicesPip = find(inPip);
            if isempty(inIndicesPip)
                notDomino = 1;
                return
            elseif ~(size(inIndicesPip)==[1 1])
                notDomino = 1;
                return
            end
        elseif dominoValue1~=0
            validPips = 0;
            % draw the polygons to test the two outer corners for pips
            for j=1:2
                if j==1
                    posNegMult2 = 1;
                else
                    posNegMult2 = -1;
                end
                posTest1 = location + posNegMult2*0.5*[length*cos(-orientation) length*sin(-orientation)];
                posTest2 = location + posNegMult2*0.15*[length*cos(-orientation) length*sin(-orientation)];
                posTest2 = posTest2 + posNegMult*vector90Norm*0.725*length;
                posTest1 = posTest1 + posNegMult*vector90Norm*0.725*length;
                posTest4 = posTest1 + posNegMult*vector90Norm*0.41*length;
                posTest3 = posTest2 + posNegMult*vector90Norm*0.41*length;
                xPlotTest1 = [posTest1(1) posTest2(1) posTest3(1) posTest4(1) posTest1(1)];
                yPlotTest1 = [posTest1(2) posTest2(2) posTest3(2) posTest4(2) posTest1(2)];
                xqPip = (currentPipLocations(:,1))';
                yqPip = (currentPipLocations(:,2))';
                inPip = inpolygon(xqPip,yqPip,xPlotTest1,yPlotTest1);
                inIndicesPip = find(inPip);
                if isempty(inIndicesPip)
                    %
                elseif size(inIndicesPip)==[1 1]
                    validPips = validPips + 1;
                end
            end
            if (dominoValue1==2 && validPips~=1)
                notDomino = 1;
                return
            elseif (validPips~=2 && dominoValue1>2)
                notDomino = 1;
                return
            end
        else
            mask = poly2mask(double(xPlot1), double(yPlot1), 1080, 1920);
            newB = imHSV(mask);
            whiteIntensity = mean(newB);
            % ditched this because it resulted in possible false positives or false negatives. Decided to forgo the 0,0 pair domino
            if 1%whiteIntensity > 0.12 && whiteIntensity < 0.2
                %
            else
                notDomino = 1;
                return
            end
        end
        if notDomino
            return
        end
        % find center of first half
        A = xPlot1(1:end-1).*yPlot1(2:end)-xPlot1(2:end).*yPlot1(1:end-1);
        As = sum(A)/2;
        x_bar = (sum((xPlot1(2:end)+xPlot1(1:end-1)).*A)*1/6)/As;
        y_bar = (sum((yPlot1(2:end)+yPlot1(1:end-1)).*A)*1/6)/As;
        if i==1
            x_bar1 = x_bar;
            y_bar1 = y_bar;
            xPlotFirst = xPlot1;
            yPlotFirst = yPlot1;
            dominoValueFirst = dominoValue1;
        else
            x_bar2 = x_bar;
            y_bar2 = y_bar;
            xPlotSecond = xPlot1;
            yPlotSecond = yPlot1;
            dominoValueSecond = dominoValue1;
        end
    end
    if dominoValueFirst == 0 && dominoValueSecond == 0
        return
    end
    plot(xPlotFirst,yPlotFirst,'b','LineWidth',2);
    text(double(x_bar1), double(y_bar1), num2str(dominoValueFirst),'FontSize', 15, 'Color', 'red', 'HorizontalAlignment', 'center');
    plot(xPlotSecond,yPlotSecond,'r','LineWidth',2);
    text(double(x_bar2), double(y_bar2), num2str(dominoValueSecond),'FontSize', 15, 'Color', 'red', 'HorizontalAlignment', 'center');
    dominoValueSame = 0;
    if dominoValueSecond<dominoValueFirst
        y_pos_lower_value = y_bar2;
        y_pos_upper_value = y_bar1;
    elseif dominoValueFirst<dominoValueSecond
        y_pos_lower_value = y_bar1;
        y_pos_upper_value = y_bar2;
    else
        dominoValueSame = 1;
    end
    angleDomino = rad2deg(orientation);
    returnAngle = angleDomino;
    angleDomino = 0;
    if ~dominoValueSame
        if y_pos_lower_value>y_pos_upper_value
            %angleDomino = angleDomino + 180;
            angleDomino = -180;
        else
            angleDomino = 0;
        end
        % turns out motor code can return to absolute angles instead of relative angles so the below is unneeded
%         if angleDomino > 180
%             angleDomino = angleDomino - 360; % -(360-angleDomino)
%         elseif angleDomino < -180
%             angleDomino = 360 + angleDomino; % 360-abs(angleDomino)
%         end
    end
    % text(double(location(1)), double(location(2)), num2str(returnAngle),'FontSize', 15, 'Color', 'red', 'HorizontalAlignment', 'center');
    if dominoValueFirst<dominoValueSecond
        returnDomino1 = dominoValueFirst;
        returnDomino2 = dominoValueSecond;
    else
        returnDomino1 = dominoValueSecond;
        returnDomino2 = dominoValueFirst;
    end
    returnArray = [location returnAngle returnDomino1 returnDomino2 angleDomino];
end
